package com.iengagelabs.supercharactercodex.repos;

import com.iengagelabs.supercharactercodex.models.SuperCharacter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class SuperCharacterRepo {

    private List<SuperCharacter> theDCUniverse = new ArrayList<>();

    private void bootStrapCharacters() {
        SuperCharacter batMan = SuperCharacter.builder()
                .id("dcu-0001")
                .name("Batman")
                .age(30)
                .build();
        SuperCharacter superMan = SuperCharacter.builder()
                .id("dcu-0002")
                .name("Superman")
                .age(50)
                .build();

        this.theDCUniverse.add(batMan);
        this.theDCUniverse.add(superMan);
    }
    public List<SuperCharacter> getAllCharacters() {
        return this.theDCUniverse;
    }


}
