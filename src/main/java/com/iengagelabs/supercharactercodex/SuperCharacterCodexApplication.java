package com.iengagelabs.supercharactercodex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SuperCharacterCodexApplication {

	public static void main(String[] args) {
		SpringApplication.run(SuperCharacterCodexApplication.class, args);
	}

}
